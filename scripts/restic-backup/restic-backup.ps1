param (
  [string]$config_dir=".restic-backup"
)

if (!(Test-Path -Path $config_dir)) {
  Write-Host "Povide config files under $config"
  exit
}

Get-ChildItem -path $config_dir -Recurse -Force | foreach {$_.attributes = "Hidden"}


$c = Get-Content "$config_dir/config.json" | ConvertFrom-Json

if (-not $env:RESTIC_REPOSITORY) {
  $env:RESTIC_REPOSITORY = $c.repository
}

$passwd_file = "$config_dir/secret.txt"
if ((Test-Path -Path $passwd_file) -and -not $env:RESTIC_PASSWORD_FILE) {
  $env:RESTIC_PASSWORD_FILE = $passwd_file
}

$cmd = "restic backup $($c.opts)"
Invoke-Expression $cmd
